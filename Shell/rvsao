echo The tasks of the IRAF RVSAO spectrum package may be run from the
echo Unix/Linux command line as if you were inside IRAF.  The parameters
echo used come from the IRAF home directory defined by the irafhome
echo environment variable or if that is not present, the IRAF/ subdirectory
echo of the user\'s home directory.
echo 
echo "bcvcorr   Compute barycentric velocity correction for a spectrum"
echo "contpars  Set continuum fitting parameters for xcsao and emsao"
echo "contsum   Set continuum fitting parameters for sumspec"
echo "emsao     Find velocities \(red shifts\) of emission lines in a spectrum"
echo "pemsao    Find velocities \(red shifts\) of emission lines in a spectrum"
echo "emplot    Plot a spectrum labelling emission and absorption lines"
echo "eqwidth   Find equivalent widths of emission and absorption lines in a spectrum"
echo "ihelp     Print help file of an IRAF task"
echo "irafhelp  Print help file of an IRAF task"
echo "linespec  Make an emission line template spectrum given a line list"
echo "listspec  List the spectrum in the specified format"
echo "qplot     Check a spectrum cross-correlation"
echo "sumspec   Add spectra, shift them, and/or remove continua"
echo "xcsao     Cross-correlate a spectrum against a set of templates"
echo "pxcsao    Cross-correlate a spectrum against a set of templates"
echo
echo If you type the command with no arguments, you will get a simple help
echo message with options for parameter access and more complete help,
echo for example:
echo
echo > xcsao
echo XCSAO: Cross-correlate a spectrum against a set of templates
echo usage: xcsao spectrum [templates=xxx][other arguments]
echo "       xcsao help  for IRAF help"
echo "       xcsao dpar  to dump parameters"
echo "       xcsao epar  to edit parameters"
echo "       xcsao lpar  to list parameters"
echo
echo where
echo [task] dpar dumps the parameters using IRAFs dpar as task.parameter=value
echo [task] epar edits the parameters using IRAFs eparam
echo [task] help opens IRAF help for the task
echo [task] lpar list the parameters using IRAFs lparam
