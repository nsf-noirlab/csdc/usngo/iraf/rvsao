RVSAO -- An IRAF package to obtain radial velocities from spectra
 
The package RVSAO defined in this directory was developed by Jessica Mink at
the Harvard-Smithsonian Center for Astrophysics from the redshift package
originally written by Gerard Kriss at Johns Hopkins University and modified
significantly by Steve Levine at the University of Wisconsin.  It obtains
radial velocities and velocity dispersions using cross-correlation methods
or emission line fits.  It consists of several SPP tasks:
XCSAO and PXCSAO cross-correlate spectra and compute a redshift,
EMSAO and PEMSAO find emission lines in spectra and compute a redshift,
BCVCORR computes the solar system barycentric velocity correction for a spectrum,
SUMSPEC adds and/or modifies spectra,
LINESPEC creates synthetic emission line templates,
EQWIDTH computes equivalent widths of lines in spectra,
WLRANGE returns the wavelength overlap range for a list of spectra
LISTSPEC lists pixel, wavelength, delta wavelength, and/or pixel value for spectrum image
PIX2WL computes the wavelength at a given pixel in a spectrum
WL2PIX computes the pixel at a given wavelength in a spectrum
VELSET changes the redshift of (log-wavelength) spectra
plus several CL scripts.

The Shell/ subdirectory contains CL scripts which include invocation of IRAF
and cn be run from the command line. If you put this directory in your search
path, you will have Unix command-line access to just about all of RVSAO's IRAF
tasks. Typing rvsao runs the script in that directory gives a list of commands
and a fairly complete explanation of how this works. Typing any of these shell
commands without an argument gives a list of script-specific arguments.

If you have any problems, please contact Jessica Mink, jmink@cfa.harvard.edu.

A task, RELEARN, has been provided to aid in updating parameters.  Run it
instead of UNLEARN to keep your current parameters settings while adding
new parameters.  It can fail when parameters are dropped; just re-run it
until it works, and things will be OK. It is also an option in any of the
shell scripts.

A set of templates commonly used at the CfA is included in the templates/
subdirectory.  The README file contains some documentation about them.
There is more in 

After the Revision Notes, you will find installation instructions.

A fairly complete description of this package has been published in the
August 1998 issue of the Publications of the Astronomical Society of the
Pacific:
https://ui.adsabs.harvard.edu/abs/1998PASP..110..934K/abstract

To see how we use this package in our production pipeline, check out our
paper in the January 2021 Astronomical Journal:
https://ui.adsabs.harvard.edu/abs/2021AJ....161....3M/abstract

For later publications, see http://tdc-www.harvard.edu/iraf/rvsao/papers/

-Jessica Mink, SAO Telescope Data Center, March 15, 2022

***************************************************************************

RVSAO Revision Log

2.8.5, March 15, 2022
Shell directory tasks updated to use IRAF2.17
Installation instructions updated to IRAF2.17

2.8.4, February 5, 2021
EMSAO: Save individual apertures/orders when they are changed
emfit.x,emplot.x,xcfit.x,xcplot.x: Pass spectrum, specim in
comon/rvspec/ so that emplot can write to file(s)
Shell directory tasks updated to use IRAF2.16.1

2.8.3, July 6, 2015
* All .com and .h files are  in rvsao$lib only
BCVCORR: Fix bug keeping JD from parameter file
EMSAO,PEMSAO: Initialize savespec to FALSE
LISTSPEC: Bug fixed so appropriately-named file is written if requested
xcplot.x: Set qplot=TRUE if spectrum is edited

2.8.2, June 13, 2014
* Edit cosmic rays out of a spectrum by resetting a point at a time
* (using the , key) or by linearly interpolating between points on
* either side of a cosmic ray (using the d key twice).  The edited
* spectrum is automatically written out if you are running qplot or
* are saving information to the spectrum header and leave with "q".
XCSAO,EMSAO,PXCSAO,PEMSAO: Write back edited spectrum
xcfit.x: Call putspec to update spectrum after editing it (2014-05-02)
emplot.x: Fix bug which reversed red and blue limits (2013-03-15)
emplot.x: Add , command to replace individual pixel values (2014-05-01)
emplot.x: Add > command to write out edited spectrum (2014-05-02)
t_eqwidth.x: Drop declaration of results.com; it's not used

2.8.1, May 25, 2012
PEMSAO: Add maxlength parameter to set output spectrum file length in mode 11
xcrslts.x: add parameter maxlength to allow longer filename in mode 11

2.8.0, April 13, 2012
LISTSPEC: Add velform format for velocity per pixel output
SKYPLOT: Base on PEMSAO instead of XCSAO, like EMPLOT
juldate.x: Do not recompute HJD if already in header
juldate.x: Check for BJD as well as HJD

2.8.1, May 25, 2012
PEMSAO: Add maxlength parameter to set output spectrum file length in mode 11
xcrslts.x: add parameter maxlength to allow longer filename in mode 11

2.8.0, April 13, 2012
LISTSPEC: Add velform format for velocity per pixel output
SKYPLOT: Base on PEMSAO instead of XCSAO, like EMPLOT
juldate.x: Do not recompute HJD if already in header
juldate.x: Check for BJD as well as HJD
vcombine.x: Do not add constant dispersion error of 15 km/sec any more
xcfile.x: Only include file name in xc file if given full pathname
xcfile.x: Drop ".ms.fits" and ".fits" from object and template filenames
xcfile.x: Add ".xcor" file extension to output filename
xcfile.x: Fix bug by redimensioning title from SZ_LINE to SZ_PATHNAME
Add new shell script rvsaopar to set RVSAO task parameters from a dpar file
Add new shell script pxcsao to execute PXCSAO

See the NEWS file for the earlier history of the software.

***************************************************************************

Here is how to install the RVSAO package in your local IRAF system
 
1)	The package is distributed as a tar archive; IRAF is distributed
	with a tar reader.  The tar archive may be obtained by magnetic
	tape or anonymous ftp.  For magnetic tape go to step [2] and when
	reading the tar archive simply mount the tape and use the tape
	device name for the archive name in step [4].  To obtain the package
	via http, go to http://tdc-www.harvard.edu/iraf/rvsao , click
	on "Download to Install", save the package gzipped tar file to any
	reasonable directory <tardir>, and follow the rest of these directions.
 
2)	Unpack the tar file into the IRAF external package root directory.
	In IRAF 2.16 or IRAF 2.17, this is ${iraf}extern/ . 
	On a UNIX system, where <tardir> is the pathname of the directory
	into which the package was downloaded and <extdir> the IRAF external
	package root directory:

	    % cd <extdir>
	    % zcat <tardir>/rvsao-2.8.5.tar.gz | tar xvf -
	    % ln -s rvsao-2.8.5 rvsao

	The archive file can be deleted once the package has been
	successfully installed.

3)	For IRAF 2.16.1 or IRAF2.17, add this link in IRAF so that RVSAO can find
	the IRAF spectrum WCS library

	    % cd ${iraf}unix/hlib/libc
	    % ln -s ../../../noao/lib/smw.h smw.h

	For earlier versions of IRAF, change the relink option in
	the rvsao/mkpkg command from

	    relink:
	    #	!mkpkg -p noao -p rvsao nrelink
	    	!mkpkg nrelink
		;
	to
	    relink:
	    	!mkpkg -p noao -p rvsao nrelink
	    #	!mkpkg nrelink
		;

4)      Start IRAF from your IRAF home directory using your usual IRAF shell command

	    % cd [irafhome]
	    % ecl

5)	Go to the RVSAO directory

	    ecl > cd rvsao
	    ecl > rvsao

6)	Using the appropriate mkpkg command below, symbolically link the
	appropriate binary directory to bin/.
	(bin/ is originally linked to bin.generic/ for distribution.)

	On a Linux PC running Linux version 3 or later
	    ecl> mkpkg linux64

	On a Linux PC running Linux version 2 or earlier
	    ecl> mkpkg linux

        On a PC running Redhat Linux, type
	    ecl> mkpkg redhat

        On an Apple Macintosh running OS X on an Intel processor, type
	    ecl> mkpkg macintel

        On an Apple Macintosh running OS X, type
	    ecl> mkpkg macosx

        On a PC running Cygwin, type
	    ecl> mkpkg cygwin

        On a SPARCstation running Solaris, type
	    ecl> mkpkg ssun

	For other architectures, make a directory of the proper bin.{arch}
	name, and link it to bin

7)      Make the package by typing
	    ecl> mkpkg update >&mkpkg.log

	The log file should be reviewed upon completion to make sure
	there were no errors.

8)	In addition to the IRAF help pages for each task, there is
	up-to-date hypertext help available on the World Wide Web at
	http://tdc-www.harvard.edu/iraf/rvsao/

9)	Scripts to run many RVSAO tasks from the Linux/Unix command line
	are in the rvsao$Shell/ subdirectory. To customize them for your
	system, change the path to the CL executable in the unix/bin.[ARCH]
	directory on the first line of each of the scripts there, and add
	the Shell directory pathname to your shell search path.

-Jessica Mink, March 15, 2022
 http://tdc-www.harvard.edu/mink 

