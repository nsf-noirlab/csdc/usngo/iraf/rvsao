#!/data/oir/IRAF2.16.1/bin.linux64/ecl.e -f
# This script runs IRAF RVSAO.QPLOT from a Unix command line
# Replace the path in the first line with one appropriate to your machine

# December 28, 2020
# By Jessica Mink

string uprm, hm, tempcom, parfile, arg4

# Set machine type appropriately
set arch=".linux64"

# Set the terminal type.
if (envget("TERM") == "sun") {
    stty gterm
} else if (envget("TERM") == "xterm") {
    stty xterm
} else if (envget("TERM") == "network") {
    stty vt100
} else {
    stty xterm
}

# Set the IRAF home and parameter directories
if (defvar("irafhome")) {
    hm = envget("irafhome")
    i = strlen(hm)
    if (substr(hm, i, i) == "/")  
        set (home = hm)
    else
        set (home = hm // "/")
    set uparm = "home$uparm/"
    }
else {
    hm = envget ("HOME")
    set (home = hm // "/IRAF/")
    set uparm = "home$uparm/"
    }

if (defvar("UPARM")) {
    uprm = envget("UPARM")
    i = strlen(uprm)
    if (substr(uprm, i, i) == "/")  
        set (uparm = uprm)
    else
        set (uparm = uprm // "/")
  }

# Set the user name which may appear on graphical output
if (envget("USER") != "") {
    set user = envget("USER")
    }

arg4 = substr(args, 1, 4)

if (args == "") {
    print ("QPLOT: Check a spectrum cross-correlation")
    print ("usage: qplot spectrum [templates=xxx][other arguments]")
    print ("       qplot help  for IRAF help")
    print ("       qplot dpar  to dump parameters")
    print ("       qplot epar  to edit parameters")
    print ("       qplot lpar  to list parameters")
    print ("       qplot rpar <name> to read parameters")
    }
else if (args == "help") {
    tempcom = mktemp ("tempqplot")
    print ("help qplot", > tempcom)
    cl (< tempcom)
    delete (tempcom)
    }

# Edit qplot parameters using epar
else if (args == "epar") {
    tempcom = mktemp ("tempqplot")
    print ("rvsao motd-\nepar qplot", > tempcom)
    cl (< tempcom)
    delete (tempcom)
    }

# Dump qplot parameters using dpar
else if (args == "dpar") {
    tempcom = mktemp ("tempqplot")
    print ("rvsao motd-\ndpar qplot", > tempcom)
    cl (< tempcom)
    delete (tempcom)
    }

# List qplot parameters using lpar
else if (args == "lpar") {
    tempcom = mktemp ("tempqplot")
    print ("rvsao motd-\nlpar qplot", > tempcom)
    cl (< tempcom)
    delete (tempcom)
    }

else if (arg4 == "rpar") {
    tempcom = mktemp ("tempqplot")
    parfile = substr(args,6,100)
    print ("rvsao motd-\ncl < ",parfile, > tempcom)
    cl (< tempcom)
    delete (tempcom)
    }

# Execute qplot using the arguments on the command line
else {
    tempcom = mktemp ("tempqplot")
    printf ("rvsao motd-\nqplot %s\n", args, > tempcom)
    cl (< tempcom)
    delete (tempcom)
    }
logout

# Oct 05 2017	Add parameter file access
# Dec 28 2020	Switch from IRAF 2.14.1 to IRAF 2.16.1
