Here is how to install the RVSAO package in your local IRAF system
 
1)	The package is distributed as a tar archive; IRAF is distributed
	with a tar reader.  The tar archive may be obtained by magnetic
	tape or anonymous ftp.  For magnetic tape go to step [2] and when
	reading the tar archive simply mount the tape and use the tape
	device name for the archive name in step [4].  To obtain the package
	via http, go to http://tdc-www.harvard.edu/iraf/rvsao , click
	on "Download to Install", save the package gzipped tar file to any
	reasonable directory <tardir>, and follow the rest of these directions.
 
2)	Unpack the tar file into the IRAF external package root directory.
	In IRAF 2.16 or IRAF 2.17, this is ${iraf}extern/ . 
	On a UNIX system, where <tardir> is the pathname of the directory
	into which the package was downloaded and <extdir> the IRAF external
	package root directory:

	    % cd <extdir>
	    % zcat <tardir>/rvsao-2.8.5.tar.gz | tar xvf -
	    % ln -s rvsao-2.8.5 rvsao

	The archive file can be deleted once the package has been
	successfully installed.

3)	For IRAF 2.16.1 or IRAF2.17, add this link in IRAF so that RVSAO can find
	the IRAF spectrum WCS library

	    % cd ${iraf}unix/hlib/libc
	    % ln -s ../../../noao/lib/smw.h smw.h

	For earlier versions of IRAF, change the relink option in the
        rvsao/mkpkg command from

	    relink:
	    #	!mkpkg -p noao -p rvsao nrelink
	    	!mkpkg nrelink
		;
	to
	    relink:
	    	!mkpkg -p noao -p rvsao nrelink
	    #	!mkpkg nrelink
		;

4)      Start IRAF from your IRAF home directory using your usual IRAF shell command

	    % cd [irafhome]
	    % ecl

5)	Go to the RVSAO directory

	    ecl > cd rvsao
	    ecl > rvsao

6)	Using the appropriate mkpkg command below, symbolically link the
	appropriate binary directory to bin/.
	(bin/ is originally linked to bin.generic/ for distribution.)

	On a Linux PC running Linux version 3 or later
	    ecl> mkpkg linux64

	On a Linux PC running Linux version 2 or earlier
	    ecl> mkpkg linux

        On a PC running Redhat Linux, type
	    ecl> mkpkg redhat

        On an Apple Macintosh running OS X on an Intel processor, type
	    ecl> mkpkg macintel

        On an Apple Macintosh running OS X, type
	    ecl> mkpkg macosx

        On a PC running Cygwin, type
	    ecl> mkpkg cygwin

        On a SPARCstation running Solaris, type
	    ecl> mkpkg ssun

	For other architectures, make a directory of the proper bin.{arch}
	name, and link it to bin

7)      Make the package by typing
	    ecl> mkpkg update >&mkpkg.log

	The log file should be reviewed upon completion to make sure
	there were no errors.

8)	In addition to the IRAF help pages for each task, there is
	up-to-date hypertext help available on the World Wide Web at
	http://tdc-www.harvard.edu/iraf/rvsao/

9)	Scripts to run many RVSAO tasks from the Linux/Unix command line
	are in the rvsao$Shell/ subdirectory. To customize them for your
	system, change the path to the CL executable in the unix/bin.[ARCH]
	directory on the first line of each of the scripts there, and add
	the Shell directory pathname to your shell search path.

-Jessica Mink, March 15, 2022
 http://tdc-www.harvard.edu/mink 

