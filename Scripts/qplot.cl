# File rvsao/Scripts/qplot.cl
# March 25, 2015
# By Jessica Mink, Harvard-Smithsonian Center for Astrophysics

# QPLOT -- Use EMSAO to plot emission and absorption lines already found
#            in a spectrum file or list of spectra

procedure qplot (spectra)

string	spectra=""	{prompt="RFN or file"}
string	qtask="xcsao"	{prompt="program to run (xcsao or emsao)"}
string	velplot="combination"	{prompt="Velocity to plot",
				 enum="combination|emission|correlation"}
bool	debug=no	{prompt="print debugging information (yes or no)"}

begin

if (qtask == "emsao") {
    emsao (spectra,vel_init="combination",linefit=no,curmode=yes,save_vel=yes,vel_plot=velplot,debug=debug)
    }
else {
    xcsao (spectra,correlate=no,curmode=yes,dispmode=2,save_vel=yes,vel_plot=velplot,debug=debug)
    }
 
end

# Sep 28 1995	New script

# Jul 16 1999	Fix so headers are updated properly
# Aug 19 1999	Add velplot parameter

# Mar 25 2015	Add debug parameter
